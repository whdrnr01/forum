@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="#">{{ $thread->creator->name }}</a> posted:
                    {{ $thread->title }}
                    <hr>
                    <p>{{ $thread->body }}</p>
                </div>
            </div>
        </div>
    </div>
    
    @if (auth()->check())
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="{{ $thread->path().'/replies' }}" method="POST">
                {{ csrf_field() }}
                    <div class="form-group">
                        <textarea name="body" id="body" class="form-control" placeholder="Somethig to say?" rows="5"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Post</button>
                </form>
                <hr>
            </div>      
        </div>
    @else 
        <p class="text-center">Please <a href="{{ route('login') }}">sign in</a> to participate in this discussion.</p>
    @endif
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @foreach ($thread->replies as $reply)
                @include ('threads.reply')
            @endforeach
        </div>
    </div>    
</div>
@endsection
